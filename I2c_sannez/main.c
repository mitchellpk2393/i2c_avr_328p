/*
 * I2c_sannez.c
 *
 * Created: 5/22/2019 1:37:07 PM
 * Author : Mitchell.K
 */ 

#include <avr/io.h>
#include "uart.h"
#include <util/twi.h>
#include <stdlib.h>
#define F_CPU 16000000
#include <util/delay.h>


#define TWBR_CALC(speed) ( (F_CPU / speed) - 16UL ) / 2UL
#define SLA_W(address)  (address << 1)
#define SLA_R(address)  ((address << 1) + 0x01)

void twi_init(uint8_t twbr_value);
void twi_init_presc(uint8_t twbr_value, uint8_t prescaler);
uint8_t twistart(void);
void twistop(void);
uint8_t twiwrite(uint8_t data);
uint8_t twiread(uint8_t ack);

uint8_t twiread_ACK(void);
uint8_t twiread_NACK(void);
char buffer[30] = {0};

int main(void)
{	
	uart0_init(8); //115200 bau
	twi_init_presc(18,4); //SCL 100khz
	
    /* Replace with your application code */
    while (1) 
    {
		uart0_puts("Scanning for devices on i2c bus ...\r\n\n");
		for(uint8_t s = 0; s <= 0x7F; s++)
		{
			twistart();
			twiwrite(SLA_W(s));
			
			if(TW_STATUS == TW_MT_SLA_ACK )
			{
				uart0_puts("Found device at address - 0x");
				itoa(s,buffer,16);
				uart0_puts(buffer);
			}
			twistop();
		}
		
    }
}

//******************************************************************
//Function  : To initialize TWI bus.
//Arguments : 1. Calculated twbr value.
//Return    :    none
//note      : Use TWBR_CALC(speed) macro to calculate twbr value.
//******************************************************************
	void twi_init(uint8_t twbr_value)
	{
		//TWSR = 0x00;
		TWBR = twbr_value;
		
	}
//******************************************************************
//Function  : To initialize TWI bus.
//Arguments : 1. Calculated twbr value.
//          : 2. Prescaler for TWI clock (1,4,16,64)
//Return    :    none
//note      : Use TWBR_CALC(speed)/prescaler macro to calculate twbr value.
//******************************************************************
	void twi_init_presc(uint8_t twbr_value, uint8_t prescaler)
	{
		TWBR = twbr_value;
		if (prescaler == 1) TWSR=(0<<TWPS1)|(0<<TWPS0);
		if (prescaler == 4) TWSR=(0<<TWPS1)|(1<<TWPS0);
		if (prescaler == 16) TWSR=(1<<TWPS1)|(0<<TWPS0);
		if (prescaler == 64) TWSR=(1<<TWPS1)|(1<<TWPS0);
		
	}

//******************************************************************
//Function  : Sends start condition.
//Arguments : none
//Return    : none
//******************************************************************
	uint8_t twistart(void)
	{
		TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
		while (!(TWCR & (1<<TWINT)));
		return 0;
	}

//******************************************************************
//Function  : Sends stop condition.
//Arguments : none
//Return    : none
//******************************************************************
	void twistop(void)
	{
		TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN);
		while ((TWCR & (1<<TWSTO)));
	}

//******************************************************************
//Function  : To send single byte on TWI bus.
//Arguments : Byte to send.
//Return    : none
//******************************************************************
	uint8_t twiwrite(uint8_t data)
	{
		TWDR = data;
		TWCR = (1<<TWINT) | (1<<TWEN);
		while (!(TWCR & (1<<TWINT)));
		return 0;
	}

//******************************************************************
//Function  : To read single byte from TWI bus.
//Arguments : Loop counter 0 = NACK or ACK on any other value.
//Return    : Received byte.
//Note      : This function seems to be smaller than separate ACK'ing and NACK'ing functions.
//******************************************************************
	uint8_t twiread(uint8_t ack) // nack 0
	{
		TWCR = ack ?
		  ((1 << TWINT) | (1 << TWEN) | (1 << TWEA))
		: ((1 << TWINT) | (1 << TWEN)) ;
		
		while (!(TWCR & (1<<TWINT)));
		return TWDR;
	}

//******************************************************************
//Function  : To read single and contiunue receiving by sending ACK.
//Arguments : none
//Return    : Received byte.
//******************************************************************
	uint8_t twiread_ACK(void)
	{
		TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA);
		
		while (!(TWCR & (1<<TWINT)));
		return TWDR;
	}

//******************************************************************
//Function  : To read single and stop receiving by sending NACK.
//Arguments : none
//Return    : Received byte.
//******************************************************************
	uint8_t twiread_NACK(void)
	{
		TWCR = (1 << TWINT) | (1 << TWEN);
		
		while (!(TWCR & (1<<TWINT)));
		return TWDR;
	}
	